#include<stdio.h>
#include<string.h>

int gps_decode(char *gps)
{
    
    
    int quality, nsats, checksum;
    float lat,latsec , lon, lonsec, accuracy, alt, ellipsoid;
    int latdeg, latmin, londeg, lonmin ;
    char ns, ew;
    char timestamp[12];
    sscanf(gps, "$GPGGA,%[1234567890.],%f,%c,%f,%c,%d,%d,%f,%f,M,%f,M,,*%x",
                timestamp,
                &lat,
                &ns,
                &lon,
                &ew,
                &quality,
                &nsats,
                &accuracy,
                &alt,
                &ellipsoid,
                &checksum);
    latdeg = (int)(lat / 100) ;
    latmin = (int)(lat -latdeg * 100) ;
    latsec = (float)(int)((lat - ((int)lat)) * 60 * 100) / 100 ;
    int latp = latsec * 100 ;
    latsec = (float)(latp) / 100 ;
    londeg = (int)(lon / 100) ;
    lonmin = (int)(lon - londeg * 100) ;
    lonsec = (float)(int)((lon - ((int)lon)) * 60 * 100) / 100 ;
    int lonp = lonsec * 100 ;
    lonsec = (float)(lonp) / 100 ;
    if(ns == 'S')
    {
        latdeg = latdeg * -1 ;
    }
    
    if(ew == 'W')
    {
        londeg = londeg * -1 ;
    }
    char gpsb[100]  ;
    char *str = gps ;
    int count = 0 ;
    int c = 0 ;
    while(  gps[count] != 0)
    {
        gpsb[count] = (int)str+count ;
        count = count + 1 ;
    }
    
    count = 1 ;
    while(str[count] != '*')
    {
    
         c ^= str[count] ;
        count = count + 1 ;
    }
    
    if(c == checksum)
    {
        printf("Time: %.*s:%.*s:%s, Lat: %d deg %d' %.2f'', Long: %d deg %d' %.2f'', Alt: %.1f\n",
            //timestamp[0], timestamp[1], timestamp[2], timestamp[3], timestamp+4,
            2, timestamp, 2, timestamp+2, timestamp+4,
            latdeg, latmin, latsec, londeg, lonmin, lonsec,
            alt) ;
        
        return 1 ;
    }
    return 0 ;
}

void decode()
{
     char gps[100];
    printf("Enter GPS string: ");
    fgets(gps, 100, stdin);
    
    int quality, nsats, checksum;
    float lat, lon, accuracy, alt, ellipsoid;
    char ns, ew;
    char timestamp[12];
    sscanf(gps, "$GPGGA,%[1234567890.],%f,%c,%f,%c,%d,%d,%f,%f,M,%f,M,,*%x",
                timestamp,
                &lat,
                &ns,
                &lon,
                &ew,
                &quality,
                &nsats,
                &accuracy,
                &alt,
                &ellipsoid,
                &checksum);
    
    printf("Time: %.*s:%.*s:%s\nLat: %d deg %fM %c\nLong: %f %c\nAlt: %f\n",
            //timestamp[0], timestamp[1], timestamp[2], timestamp[3], timestamp+4,
            2, timestamp, 2, timestamp+2, timestamp+4,
            (int)(lat / 100), lat - ((int)(lat / 100)) * 100, ns,
            lon, ew,
            alt);
//Time: 12:35:19
//Lat: 48 deg 7.038086M N
//Long: 1131.000000 E
//Alt: 545.400024

}
// $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
int main()
{

    char gps[100];
    printf("Enter GPS string: ");
    fgets(gps, 100, stdin);
    int ret = gps_decode(gps);
    if (! ret)
    {
        printf ("Invalid GPS string\n");
    }

}